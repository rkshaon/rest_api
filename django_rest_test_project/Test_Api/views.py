from django.db.models import Q
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.db import IntegrityError
import datetime

from Test_Api.models import StudentInformation


@api_view(['GET'])
def get_student_information(request):
    try:
        name = request.GET.get('name') if request.GET.get('name') else None
        roll = request.GET.get('roll') if request.GET.get('roll') else None
        dep = request.GET.get('dep') if request.GET.get('dep') else None
        gpa = request.GET.get('gpa') if request.GET.get('gpa') else None
        if name or roll or dep or gpa:
            student_info = StudentInformation.objects.filter(Q(name=name) |
                                                             Q(roll=roll) |
                                                             Q(dep=dep) |
                                                             Q(gpa=gpa))
        else:
            student_info = StudentInformation.objects.all()
        return Response([{
            "id":s.id,
            "name": s.name,
            "roll": s.roll,
            "department": s.dep,
            "gpa": s.gpa,
            "time": s.time
        }for s in student_info], status=200)

    except Exception as e:
        return Response({'status': 400, 'error': e}, status=400)


@api_view(['POST'])
def create_student_information(request):
    try:
        name = request.POST.get('name') if request.POST.get('name') else None
        roll = request.POST.get('roll') if request.POST.get('roll') else None
        dep = request.POST.get('dep') if request.POST.get('dep') else None
        gpa = request.POST.get('gpa') if request.POST.get('gpa') else None
        student_info = StudentInformation(name=name, roll=roll, dep=dep, gpa=gpa)
        student_info.save()
        return Response({
            "message": "successfully created",
        }, status=200)

    except IntegrityError:
        return Response({"message": "This Department and Roll are exist"}, status=200)
    except Exception as e:
        return Response({'status': 400, 'error': e}, status=400)


@api_view(['POST'])
def update_student_information(request):
    try:
        id = request.POST.get('id') if request.POST.get('id') else None
        name = request.POST.get('name') if request.POST.get('name') else StudentInformation.objects.filter(pk=id).values('name')
        roll = request.POST.get('roll') if request.POST.get('roll') else StudentInformation.objects.filter(pk=id).values('roll')
        dep = request.POST.get('dep') if request.POST.get('dep') else StudentInformation.objects.filter(pk=id).values('dep')
        gpa = request.POST.get('gpa') if request.POST.get('gpa') else StudentInformation.objects.filter(pk=id).values('gpa')
        if id:
            try:
                student_info = StudentInformation.objects.get(pk=id)
                student_info.name = name
                student_info.roll = roll
                student_info.dep = dep
                student_info.gpa = gpa
                student_info.save()
                return Response({
                    "message": "successfully update",
                }, status=200)
            except StudentInformation.DoesNotExist:
                return Response({'status': 404, 'error': 'Student not found'}, status=404)
        else:
            return Response({'status': 412, 'error': "Student Id must be required"}, status=412)

    except IntegrityError:
        return Response({"message": "This Department and Roll are exist"}, status=200)
    except Exception as e:
        return Response({'status': 400, 'error': e}, status=400)


@api_view(['POST'])
def delete_expire_information(request):
    now = datetime.datetime.now() - datetime.timedelta(minutes=5)

    student_info = StudentInformation.objects.filter(time__lt=now).delete()
    if student_info[0] != 0:
        return Response({"message": "Expiry Data has been Deleted"})
    else:
        return Response({"message": "Expiry Data Not found"})

