from django.apps import AppConfig


class TestApiConfig(AppConfig):
    name = 'Test_Api'
