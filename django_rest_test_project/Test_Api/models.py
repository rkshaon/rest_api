from django.db import models
from django.core.validators import MaxValueValidator
import datetime


class StudentInformation(models.Model):
    name = models.CharField(max_length=50)
    roll = models.PositiveIntegerField(default=1, validators=[MaxValueValidator(100000)])
    dep = models.CharField(max_length=20)
    gpa = models.DecimalField(decimal_places=2, max_digits=3)
    time = models.DateTimeField(default=datetime.datetime.now())

    class Meta(object):
        unique_together = (('roll', 'dep'),)
